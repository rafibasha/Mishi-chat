//
//  ProfileCell.swift
//  Mishi chat
//
//  Created by Rafi Basha on 06/08/21.
//

import UIKit

class ProfileCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
