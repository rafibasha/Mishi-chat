//
//  ProfileVC.swift
//  Mishi chat
//
//  Created by Rafi Basha on 06/08/21.
//

import UIKit

class ProfileVC: UIViewController {
    @IBOutlet weak var table_profile: UITableView!
    
    let tabledate = ["Ronadlo@gmail.com","+91 993939498","Change password"]
    @IBOutlet weak var img_profile: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        img_profile.layer.cornerRadius = img_profile.frame.size.height / 2
        
        // Do any additional setup after loading the view.
        table_profile.tableFooterView = UIView()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ProfileVC: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return tabledate.count
    }
    // Make the background color show through
  
        
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell : ProfileCell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as? ProfileCell else {
            preconditionFailure("reusable cell not found")
        }
        let singleitem = tabledate[indexPath.row]
        cell.title.text = singleitem
        return cell
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
       tableView.deselectRow(at: indexPath, animated: true)
    }
  

       
}
