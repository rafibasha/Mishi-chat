//
//  ChatVCViewController.swift
//  Mishi chat
//
//  Created by Rafi Basha on 10/08/21.
//

import UIKit

class ChatVC: UIViewController {
    
    @IBOutlet weak var table_chats: UITableView!
    var SelectedBot:ChatResponse?
    var chatlist = [Chatsingleitem]()
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let data = SelectedBot?.data else {
            return
        }
        chatlist = data
        self.table_chats.rowHeight = UITableView.automaticDimension;
        self.table_chats.estimatedRowHeight = 44.0;
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChatVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let singleitem = chatlist[indexPath.row]
        if singleitem.author == "agent"
        {
            guard let cell : ChatCell = tableView.dequeueReusableCell(withIdentifier: "ChatCell2") as? ChatCell else {
                preconditionFailure("reusable cell not found")
            }
            let singleitem = chatlist[indexPath.row]
            cell.lbl_message.text = singleitem.message
            
          
            cell.lbl_message.textColor = .black
            cell.lbl_message.textAlignment = .right
            cell.lbl_message.backgroundColor = #colorLiteral(red: 0, green: 0.1638666391, blue: 0.2961188853, alpha: 1)
            cell.lbl_message.textColor = .white
            //cell.lbl_message.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner,.layerMinXMaxYCorner]
            cell.lbl_message.layer.cornerRadius = 20
            cell.lbl_message.layer.masksToBounds = true
            return cell
        }
        else{
            guard let cell : ChatCell = tableView.dequeueReusableCell(withIdentifier: "ChatCell") as? ChatCell else {
                preconditionFailure("reusable cell not found")
            }
           
            cell.lbl_message.text = singleitem.message
            
          
            cell.lbl_message.textColor = .black
            cell.lbl_message.layer.borderWidth  = 1
            //cell.lbl_message.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMaxYCorner,.layerMaxXMaxYCorner]
            cell.lbl_message.layer.cornerRadius = 20
            cell.lbl_message.layer.masksToBounds = true
            cell.lbl_name.layer.cornerRadius = 22.5
            cell.lbl_name.layer.masksToBounds = true
            return cell
        }
     
    }
   
    
    
}
