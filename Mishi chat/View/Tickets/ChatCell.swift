//
//  ChatCell.swift
//  Mishi chat
//
//  Created by Rafi Basha on 11/08/21.
//

import UIKit

class ChatCell: UITableViewCell {

    @IBOutlet weak var lbl_name: PaddingLabel!
    @IBOutlet weak var lbl_message: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
