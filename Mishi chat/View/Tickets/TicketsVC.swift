//
//  TicketsVC.swift
//  Mishi chat
//
//  Created by Rafi Basha on 06/08/21.
//

import UIKit

class TicketsVC: UIViewController {
    private var chatsViewModel = ChatViewModel()
    var ChatData = [ChatResponse]()
   
    var id:String = ""
    let cellSpacingHeight: CGFloat = 10
   
    @IBOutlet weak var view_popup: UIView!
    @IBOutlet weak var searchbar: UISearchBar!
    @IBOutlet weak var tableview_Tickets: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
      
        print("Tick",id);
        chatsViewModel.delegate = self
        view_popup.isHidden = true
        tableview_Tickets.rowHeight = 190
        searchbar.searchTextField.layer.cornerRadius = 20
        searchbar.searchTextField.layer.masksToBounds = true
        searchbar.searchTextField.layer.cornerRadius = 20
        searchbar.searchTextField.layer.masksToBounds = true
        searchbar.backgroundColor = .clear
        searchbar.barTintColor = UIColor.white
        searchbar.setBackgroundImage(UIImage.init(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        searchbar.setLeftImage(#imageLiteral(resourceName: "Search"), with: 10, tintColor: .gray)
        searchbar.textField?.textColor = UIColor.gray
        searchbar.textField?.backgroundColor = UIColor(named:"#e5e5e5")
        searchbar.changePlaceholderColor(UIColor.gray)
        searchbar.backgroundImage = UIImage(color: UIColor.white, size: CGSize(width: searchbar.frame.width, height: searchbar.frame.height))
        self.view.showLoader()
      
        let urlpath = ApiEndpoints.botsChats + id
        chatsViewModel.getChats(botRequest: urlpath)
        
    }
    @IBAction func btn_Ticker_Action(_ sender: Any) {
        view_popup.isHidden = false
    }
    
}
extension TicketsVC: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
       return ChatData.count
    }
    // Make the background color show through
  
        
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell : TicketsCell = tableView.dequeueReusableCell(withIdentifier: "TicketsCell") as? TicketsCell else {
            preconditionFailure("reusable cell not found")
        }
        let singleitem = ChatData[indexPath.row].data?.last
        cell.lbl_message.text = singleitem?.message
        return cell
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
       tableView.deselectRow(at: indexPath, animated: true)
       
        
        
        let  mainStory = UIStoryboard(name: "Main", bundle: nil)
        
     let mySettings:ChatVC = mainStory.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        
        mySettings.SelectedBot = ChatData[indexPath.row]
   
      self.navigationController?.pushViewController(mySettings, animated: true)
    }
  
    internal func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           return cellSpacingHeight
       }
       
}

extension TicketsVC : ChatViewModelDelegate
{
    func didReceiveChatsResponse(Chatdata: [ChatResponse]?) {
        print("success chats")
        guard let response = Chatdata else {
            return
        }
        self.view.removeLoader()
        ChatData = response
        self.tableview_Tickets.reloadData()
    }
    
    func didReceiveChatsFailed(Chatdata: String?) {
        self.view.removeLoader()
        print("failed chats")
    }
    
    
}
