//
//  TicketsCell.swift
//  Mishi chat
//
//  Created by Rafi Basha on 06/08/21.
//

import UIKit

class TicketsCell: UITableViewCell {

    @IBOutlet weak var lbl_message: UILabel!
    @IBOutlet weak var view_circle: UILabel!
    @IBOutlet weak var view_whatsapp: UIView!
    @IBOutlet weak var view_bg: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        view_whatsapp.clipsToBounds = true
        view_whatsapp.layer.cornerRadius = 30
      view_whatsapp.layer.maskedCorners = [.layerMinXMaxYCorner]
        view_circle.clipsToBounds = true
        view_circle.layer.cornerRadius = view_circle.frame.size.width / 2
        view_bg.layer.shadowColor = UIColor.gray.cgColor
        view_bg.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        view_bg.layer.shadowRadius = 2.0
        view_bg.layer.shadowOpacity = 1.0
        view_bg.layer.masksToBounds = false
        view_bg.layer.shadowPath = UIBezierPath(roundedRect:view_bg.bounds, cornerRadius:view_bg.layer.cornerRadius).cgPath
    }
    override func layoutSubviews() {
        super.layoutSubviews()
       
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
