//
//  Tabbar.swift
//  Mishi chat
//
//  Created by Rafi Basha on 05/08/21.
//

import Foundation
import UIKit
class CustomTabBar: UITabBar {
        
     override func awakeFromNib() {
            super.awakeFromNib()
            layer.masksToBounds = true
            layer.cornerRadius = 40
            layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        self.layer.frame = CGRect(x: 0, y: self.frame.height-200, width: self.frame.size.width, height: self.frame.size.height)
               
      }
    
 }
