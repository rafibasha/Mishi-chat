//
//  RoundedTabbarController.swift
//  Mishi chat
//
//  Created by Rafi Basha on 05/08/21.
//

import Foundation
import UIKit
class RoundedTabBarController: UITabBarController {
    
    var botid : String!
    
  override func viewDidLoad()
  {
    super.viewDidLoad()
    let vc = self.viewControllers![0] as! TicketsVC
    vc.id = botid
    let layer = CAShapeLayer()
           layer.path = UIBezierPath(roundedRect: CGRect(x: 30, y: self.tabBar.bounds.minY , width: self.tabBar.bounds.width - 60, height: self.tabBar.bounds.height + 10), cornerRadius: (self.tabBar.frame.width/2)).cgPath
           layer.shadowColor = UIColor.lightGray.cgColor
           layer.shadowOffset = CGSize(width: 5.0, height: 0.0)
           layer.shadowRadius = 25.0
           layer.shadowOpacity = 0.3
           layer.borderWidth = 1.0
           layer.opacity = 1.0
           layer.isHidden = false
           layer.masksToBounds = false
           layer.fillColor = UIColor.white.cgColor
     
           self.tabBar.layer.insertSublayer(layer, at: 0)

  }
}
