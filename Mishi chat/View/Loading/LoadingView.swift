//
//  LoginView.swift
//  MvvmDemoApp
//
//  Created by CodeCat15 on 3/15/20.
//  Copyright © 2020 Codecat15. All rights reserved.
//

import UIKit
/*struct ProgressDialog {
    static var alert = UIAlertController()
    static var progressView = UIProgressView()
    static var progressPoint : Float = 0{
        didSet{
            if(progressPoint == 1){
                ProgressDialog.alert.dismiss(animated: true, completion: nil)
            }
        }
    }
}
extension UIViewController{
   func LoadingStart(){
        ProgressDialog.alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
    
    let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
    loadingIndicator.hidesWhenStopped = true
    loadingIndicator.style = UIActivityIndicatorView.Style.gray
    loadingIndicator.startAnimating();

    ProgressDialog.alert.view.addSubview(loadingIndicator)
    present(ProgressDialog.alert, animated: true, completion: nil)
  }

  func LoadingStop(){
    ProgressDialog.alert.dismiss(animated: true, completion: nil)
  }
}*/
class LoadingView: UIView {

    var blurEffectView: UIView?
    var blurView: UIImageView?

    override init(frame: CGRect) {
        
        let blurEffectView = UIView()
        blurEffectView.frame = frame
        blurEffectView.backgroundColor = .clear
        //blurEffectView.alpha = 0.50
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.blurEffectView = blurEffectView
        super.init(frame: frame)
        addSubview(blurEffectView)
        
        let blurView = UIImageView()
        blurView.frame = frame
        blurView.image = UIImage(named: "transparent")
        blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.blurView = blurView
        addSubview(blurView)
        
        
        addLoader()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func addLoader() {
        guard let blurEffectView = blurView else { return }
        let activityIndicator = UIActivityIndicatorView(style: .large)
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        blurEffectView.addSubview(activityIndicator)
        activityIndicator.center = blurEffectView.center
        activityIndicator.startAnimating()
        activityIndicator.color = .white
    }
}
extension UIView {
    func showLoader() {
        let blurLoader = LoadingView(frame: frame)
        self.addSubview(blurLoader)
    }

    func removeLoader() {
        if let blurLoader = subviews.first(where: { $0 is LoadingView }) {
            blurLoader.removeFromSuperview()
        }
    }
}

