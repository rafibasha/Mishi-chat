//
//  LoginVC.swift
//  Mishi chat
//
//  Created by Rafi Basha on 06/08/21.
//

import UIKit

class LoginVC: UIViewController {
    private var loginViewModel = LoginViewModel()
    
    @IBOutlet weak var text_password: UITextField!
    @IBOutlet weak var text_username: UITextField!
    @IBOutlet weak var btn_login: UIButton!
    @IBOutlet weak var view_password: UIView!
    @IBOutlet weak var view_user: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        uiupdate()
        btn_login.layer.cornerRadius =  btn_login.frame.size.height / 2
        loginViewModel.delegate = self
        text_username.text = "agent1"
        text_password.text = "mishi1234"
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_Login_Action(_ sender: Any) {
        self.view.showLoader()
        let request = LoginRequest(username: text_username.text, password: text_password.text)
        loginViewModel.loginUser(loginRequest: request)
    }
    func uiupdate(){
        ui_shadow(view: view_user)
        ui_shadow(view: view_password)
    }
    func ui_shadow(view:UIView){
        view.layer.cornerRadius = 5
        view.layer.borderWidth = 1.0

        view.layer.borderColor = #colorLiteral(red: 0.7981414199, green: 0.8082280755, blue: 0.8465437889, alpha: 1)
      
    }
 

}
extension LoginVC : LoginViewModelDelegate
{
    func didReceiveLoginResponse(loginResponse: LoginResponse_json?) {
        self.view.removeLoader()
        print("Log",loginResponse?.token as Any)
        guard let date =  loginResponse else {
            return
        }
       
        if let myDelegate = (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate){
            
         
            UserDefaults.standard.set(date.org_id, forKey: "org_id")
            myDelegate.manageLoginSession()
            
        }
       
    }
    
    func didReceiveLoginFailed(loginResponse: String?) {
        self.view.removeLoader()
        let alert = UIAlertController(title: Constants.ErrorAlertTitle, message: loginResponse, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: Constants.OkAlertTitle, style: .default, handler: nil))

        self.present(alert, animated: true)
    }
    
    
}
