//
//  ViewController.swift
//  Mishi chat
//
//  Created by Rafi Basha on 05/08/21.
//

import UIKit

class botsViewController: UIViewController ,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout , TapCellDelegate, UISearchBarDelegate {
    private var botsViewModel = BotsViewModel()
    var Totalbots = [BotsResponse]()
    
    @IBOutlet weak var SearchBar: UISearchBar!
    
    @IBOutlet weak var collectionview: UICollectionView!
    
    var cellIdentifier = "Cell"
    var numberOfItemsPerRow : Int = 2
    var dataSource:[String]?
    var dataSourceForSearchResult:[String]?
    var searchBarActive:Bool = false
    var searchBarBoundsY:CGFloat?
    var refreshControl:UIRefreshControl?
    
    var cellWidth:CGFloat{
        return collectionview.frame.size.width/2
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        botsViewModel.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        SearchBar.delegate = self
        collectionview.dataSource = self
        collectionview.delegate = self
        self.dataSource = ["Agent1","Agent2","Agent3","Agent4","Agent5","Agent6","Agent7","Agent8","Agent9", "Agent10","Agent11","Agent12","Agent13","Agent14","Agent15","Agent16", "Agent17","Agent18","Agent19","Agent20"]
        self.dataSourceForSearchResult = [String]()
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
          var width = UIScreen.main.bounds.width
          layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
      width = width - 20
      layout.itemSize = CGSize(width: width / 2-10, height: width / 2-10)
          layout.minimumInteritemSpacing = 0
          layout.minimumLineSpacing = 20
        collectionview!.collectionViewLayout = layout
        SearchBar.searchTextField.layer.cornerRadius = 20
        SearchBar.searchTextField.layer.masksToBounds = true
        SearchBar.backgroundColor = .clear
        SearchBar.barTintColor = UIColor.white
        SearchBar.setBackgroundImage(UIImage.init(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        SearchBar.setLeftImage(#imageLiteral(resourceName: "Search"), with: 10, tintColor: .gray)
        SearchBar.textField?.textColor = UIColor.gray
        SearchBar.textField?.backgroundColor = UIColor(named:"#e5e5e5")
        SearchBar.changePlaceholderColor(UIColor.gray)
        SearchBar.backgroundImage = UIImage(color: UIColor.white, size: CGSize(width: SearchBar.frame.width, height: SearchBar.frame.height))
        self.view.showLoader()
        let org_id =  UserDefaults.standard.value(forKey: "org_id")
        
        let id:String = org_id as! String
        let urlpath = ApiEndpoints.bots + id + "/bots"
        botsViewModel.getbots(botRequest: urlpath)
    }
    
    // MARK: Search
    func filterContentForSearchText(searchText:String){
        self.dataSourceForSearchResult = self.dataSource?.filter({ (text:String) -> Bool in
            return text.contains(searchText)
        })
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 0 {
            self.searchBarActive    = true
            self.filterContentForSearchText(searchText: searchText)
            self.collectionview?.reloadData()
        }else{
            self.searchBarActive = false
            self.collectionview?.reloadData()
        }
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self .cancelSearching()
        self.collectionview?.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBarActive = true
        self.view.endEditing(true)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.SearchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBarActive = false
        self.SearchBar.setShowsCancelButton(false, animated: false)
    }
    func cancelSearching(){
        self.searchBarActive = false
        self.SearchBar.resignFirstResponder()
        self.SearchBar.text = ""
    }
    
    
    // MARK: <UICollectionViewDataSource>
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! CollectionViewCell
        cell.indexPath = indexPath
        cell.delegate = self
        let randomRed:CGFloat = CGFloat(arc4random_uniform(256))
        let randomGreen:CGFloat = CGFloat(arc4random_uniform(256))
        let randomBlue:CGFloat = CGFloat(arc4random_uniform(256))
        let myColor =  UIColor(red: randomRed/255, green: randomGreen/255, blue: randomBlue/255, alpha: 1.0)

        cell.topButton.backgroundColor = myColor
        //cell.layer.borderWidth = 1.0;
        //cell.layer.borderColor = UIColor.green.cgColor
        cell.topLabel.textColor = UIColor.white
        
        if (self.searchBarActive) {
            cell.topLabel!.text = self.dataSourceForSearchResult![indexPath.row];
        }else{
            let singleItem = self.Totalbots[indexPath.row]
            cell.topLabel!.text = singleItem.name
        }
        cell.contentView.layer.cornerRadius = 10
        cell.contentView.layer.borderWidth = 1.0

        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = true

        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.searchBarActive {
            return self.dataSourceForSearchResult!.count;
        }
        return self.Totalbots.count
    }
    
    func buttonTapped(indexPath: IndexPath) {
        print("success")
        let singleItem = self.Totalbots[indexPath.row]
    
        let  mainStory = UIStoryboard(name: "Main", bundle: nil)
     
        let mySettings:RoundedTabBarController = mainStory.instantiateViewController(withIdentifier: "RoundedTabBarController") as! RoundedTabBarController
           
        mySettings.title = ""
         
        mySettings.botid = singleItem.id
         self.navigationController?.pushViewController(mySettings, animated: true)
           }
    
    // MARK: <UICollectionViewDelegateFlowLayout>
    /*func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.bounds.width
              return CGSize(width: collectionViewWidth/2-20, height: collectionViewWidth/2-20)
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(numberOfItemsPerRow - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(numberOfItemsPerRow))
        return CGSize(width: size-20, height: size-20)
    }*/
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
           return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
       }
    func CGRectMake(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) -> CGRect {
        return CGRect(x: x, y: y, width: width, height: height)
    }

}

extension botsViewController : BotsViewModelDelegate
{
    func didReceiveTicketsResponse(botsResponse: [BotsResponse]?) {
        print("success bots")
        guard let bots =  botsResponse else {
            return
        }
        Totalbots = bots
        self.collectionview.reloadData()
        self.view.removeLoader()
    }
    
    func didReceiveTicketsFailed(botsResponse: String?) {
        print("failed bots")
        self.view.removeLoader()
    }
    
}
