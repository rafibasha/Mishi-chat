//
//  HttpUtility.swift
//  MvvmDemoApp
//
//  Created by Codecat15 on 3/14/2020.
//  Copyright © 2020 Codecat15. All rights reserved.
//

import Foundation
import SwiftyJSON
struct HttpUtility
{
    func getApiData<T:Decodable>(requestUrl: URL, resultType: T.Type, completionHandler:@escaping(_ result: T?)-> Void)
    {
        URLSession.shared.dataTask(with: requestUrl) { (responseData, httpUrlResponse, error) in
            if(error == nil && responseData != nil && responseData?.count != 0)
            {
                let decoder = JSONDecoder()
                do {
                    let result = try decoder.decode(T.self, from: responseData!)
                    _=completionHandler(result)
                }
                catch let error{
                    debugPrint("error occured while decoding = \(error.localizedDescription)")
                }
            }

        }.resume()
    }

    /*func postApiData<T:Decodable>(requestUrl: URL, requestBody: Data, resultType: T.Type, completionHandler:@escaping(_ result: LoginResponse, _ Error:String)-> Void)
    {
        var urlRequest = URLRequest(url: requestUrl)
        urlRequest.httpMethod = "post"
        urlRequest.httpBody = requestBody
        urlRequest.addValue("application/json", forHTTPHeaderField: "content-type")
        let string = String(data: requestBody, encoding: .utf8)!
        
        print("st",string)
        URLSession.shared.dataTask(with: urlRequest) { (data, httpUrlResponse, error) in

            if(data != nil && data?.count != 0)
            {
                
                
                do {
                    if let httpResponse = httpUrlResponse as? HTTPURLResponse {
                        print("statusCode: \(httpResponse.statusCode)")
                        if httpResponse.statusCode == 200{
                            let jsonResponse = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                            print("jsonResponse",jsonResponse)
                            //let response = try JSONDecoder().decode(T.self, from: data!)
                            _=completionHandler(response as! LoginResponse, "Success")
                        }
                        else if  httpResponse.statusCode == 401{
                            let jsonResponse = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                           
                            let response = try JSONDecoder().decode(LoginError.self, from: data!)
                            print("erroResponse",response)
                            //let response = try JSONDecoder().decode(, from: data!)
                            _=completionHandler(response, "Failed")
                        }
                    }
                    
                    
                }
                catch let decodingError {
                    debugPrint(decodingError)
                   
                }
            }
        }.resume()
    }*/
    
    func getBotsData(requestUrl: URL,  completionHandler:@escaping(_ result: [BotsResponse]?)-> Void)
    {
        URLSession.shared.dataTask(with: requestUrl) { (responseData, httpUrlResponse, error) in
            if(error == nil && responseData != nil && responseData?.count != 0)
            {
                
                do {
                    let json = try JSON(data: responseData!)
                    let dataSource = json.arrayValue.map({return  BotsResponse(id: $0["id"].stringValue, name: $0["name"].stringValue, organization: $0["organization"].stringValue)})
                    _=completionHandler(dataSource)
                }
                catch let error{
                    debugPrint("error occured while decoding = \(error.localizedDescription)")
                }
            }

        }.resume()
    }
    
    func getChatsData(requestUrl: URL,  completionHandler:@escaping(_ result: [ChatResponse]?)-> Void)
    {
        URLSession.shared.dataTask(with: requestUrl) { (responseData, httpUrlResponse, error) in
            if(error == nil && responseData != nil && responseData?.count != 0)
            {
                
                do {
                    let json = try JSON(data: responseData!)
                   
                    let Chatdata = json["chats"].dictionary
                    
                  
                    guard let Totalkeys = Chatdata?.keys else{
                        return
                    }
                    
                   //print(Totalkeys)
                    let response = Totalkeys.map({ (string) -> ChatResponse in
                                                        
                                                        let list = Chatdata![string]
                                                        let sorteditem = list?.arrayValue.map({return Chatsingleitem(author: $0["author"].stringValue, message: $0["message"].stringValue, channel: $0["channel"].stringValue, status: $0["status"].stringValue, sub_date: $0["sub_date"].stringValue)})
                                                       
                                                      // print(sorteditem)
                                                        
                                                        let sorted = ChatResponse(id: string, data: sorteditem)
                                                        return sorted})
                    //print(response)
                    
                    
                 
                    _=completionHandler(response)
                }
                catch let error{
                    debugPrint("error occured while decoding = \(error.localizedDescription)")
                }
            }

        }.resume()
    }
    func postApiData_login(requestUrl: URL, requestBody: Data, completionHandler:@escaping(_ result: LoginResponse_total)-> Void)
    {
        var urlRequest = URLRequest(url: requestUrl)
        urlRequest.httpMethod = "post"
        urlRequest.httpBody = requestBody
        urlRequest.addValue("application/json", forHTTPHeaderField: "content-type")
        let string = String(data: requestBody, encoding: .utf8)!
        
        print("st",string)
        URLSession.shared.dataTask(with: urlRequest) { (data, httpUrlResponse, error) in

            if(data != nil && data?.count != 0)
            {
                
                
                do {
                    if let httpResponse = httpUrlResponse as? HTTPURLResponse {
                        print("statusCode: \(httpResponse.statusCode)")
                        let json = try JSON(data: data!)
                       
                        print("jsonResponse",json)
                        if httpResponse.statusCode == 200{
                            
                            
                            let response = LoginResponse_json(token: json["token"].stringValue, userId: json["userId"].stringValue, username: json["username"].stringValue, firstname: json["firstname"].stringValue, lastname: json["lastname"].stringValue, is_staff: json["is_staff"].boolValue, organization: json["organization"].stringValue, org_id: json["org_id"].stringValue)
                            
                            let totalresponse = LoginResponse_total(message: "Success", data: response)

                            //let response = try JSONDecoder().decode(T.self, from: data!)
                            _=completionHandler(totalresponse)
                        }
                        else if  httpResponse.statusCode == 401{
                            let totalresponse = LoginResponse_total(message: json["message"].stringValue, data: nil)

                            //let response = try JSONDecoder().decode(, from: data!)
                            _=completionHandler(totalresponse)
                        }
                    }
                    
                    
                }
                catch let decodingError {
                    debugPrint(decodingError)
                   
                }
            }
        }.resume()
    }
}
