//
//  Onbording.swift
//  Mishi
//
//  Created by Rafi Basha on 05/07/21.
//

import UIKit
import SCPageControl
class OnbordingViewController: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var btn_Skip: UIButton!
    @IBOutlet weak var sc_main: UIScrollView!
    @IBOutlet weak var ScPageControl: SCPageControlView!
   
    
    var screenWidth : CGFloat = UIScreen.main.bounds.size.width
    var screenHeight : CGFloat = UIScreen.main.bounds.size.height
    
   
    
    let arrdic = [["title":"Mobile first Support",
                   "subtitle":"Stay connected with your loved Brands.Buy products directly from your favourite Brands","image":"onboard1"],
                  ["title":"Secured Communication","subtitle":"Book Appointments, Make Reservations ..etc","image":"onboard2"], ["title":"Omni Channel","subtitle":"Book Appointments, Make Reservations ..etc","image":"onboard3"]
                 ]
    
   
    let arr_color: [UIColor] = [UIColor.red, UIColor.blue, UIColor.green, UIColor.purple, UIColor.brown]
    var previousDeviceOrientation: UIDeviceOrientation = UIDevice.current.orientation
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        sc_main.showsHorizontalScrollIndicator = false
        sc_main.showsVerticalScrollIndicator = false
//        NotificationCenter.default.addObserver(
//            self,
//            selector:  #selector(deviceDidRotate),
//            name: UIDevice.orientationDidChangeNotification,
//            object: nil
//        )
        init_view()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //if you use autolayout, initial SCPageControll in ViewDidLayoutSubviews Method
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        /*
         ScPageControl.frame = ScPageControl.frame
         ScPageControl.scp_style = .SCNormal
         ScPageControl.set_view(Int(sc_main.contentSize.width/screenWidth), current: 0, tint_color: .red)
         */
    }
    @IBAction func btn_Skip(_ sender: Any) {
//        progress = progress + 0.25
//        circularProgress.setProgress(to: progress, withAnimation: true)
       
        if let myDelegate = (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate){
            
            
            UserDefaults.standard.set("Mishichat", forKey: "Onbaord_Skip")
            myDelegate.manageLoginSession()
            
        }
    }
    
    //MARK: ## view init ##
    func init_view() {
        
        sc_main.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        sc_main.delegate = self
        sc_main.isPagingEnabled = true
        sc_main.contentSize = CGSize(width: screenWidth*CGFloat(arrdic.count), height: screenHeight)
        //sc_main.contentOffset = CGPoint(x: sc_main.frame.size.width, y: 0) //<- Only Start Page Not first Page
        
        print(Int(sc_main.contentSize.width/screenWidth))
        for i in 0 ..< Int(sc_main.contentSize.width/screenWidth) {
            let singledict = arrdic[i]
           
            let micrheight = 60
            let max_height = calculatePercentage(max_width: Double(screenHeight),percentageVal:  Double(micrheight))
            let micrheight2 = 20
            let max_height2 = calculatePercentage(max_width: Double(screenHeight),percentageVal:  Double(micrheight2))
            let max_width = calculatePercentage(max_width: Double(screenWidth),percentageVal:  Double(micrheight))
            //let xposition = screenWidth*CGFloat(i)
            //print("xposition",screenWidth*CGFloat(i))
            //let screenwidth_split = screenWidth*CGFloat(i)
            /*let screenHeight_split = screenHeight / 2
            let screenwidht_split = screenWidth / 2
            let ivImage_split = max_width / 2
            let ivImage_postion = CGFloat(screenwidht_split) - CGFloat(ivImage_split)
           
           
            let yposition = CGFloat(screenHeight_split) - CGFloat(ivImage_split)
            let ivImage = UIImageView(frame: CGRect(x: Double(xposition + ivImage_postion ), y: Double(yposition), width: max_width, height: max_width))
            ivImage.image = arrImg[i]
            ivImage.contentMode = .scaleAspectFit
            ivImage.clipsToBounds = true
            sc_main.addSubview(ivImage)*/
            let View_subView = UIView(frame: CGRect(x: screenWidth*CGFloat(i), y:CGFloat(max_height2), width: screenWidth, height: CGFloat(max_height)))
                                      
            // View_subView.backgroundColor = arr_color[i]
            sc_main.addSubview(View_subView)
            let screenwidth_split = View_subView.frame.size.width / 2
            let ivImage_split = max_width / 2
            
            let ivImage = UIImageView(frame: CGRect(x: Double(screenwidth_split) -  ivImage_split , y:0, width: max_width, height: max_width))
            ivImage.image = UIImage(named: singledict["image"]!)
            ivImage.contentMode = .scaleAspectFit
            ivImage.clipsToBounds = true
            View_subView.addSubview(ivImage)
            let lb_title = UILabel(frame: CGRect(x: 0, y: 250, width: View_subView.frame.size.width, height: 100))
             //lb_subtitle.text = "\(i+1) Page"
            lb_title.textAlignment = .center
            lb_title.numberOfLines = 0
            lb_title.text = singledict["title"]
            lb_title.font = UIFont (name: "Poppins-Regular", size: 20)
            View_subView.addSubview(lb_title)
             let lb_subtitle = UILabel(frame: CGRect(x: 0, y: 300, width: View_subView.frame.size.width, height: 100))
             //lb_subtitle.text = "\(i+1) Page"
             lb_subtitle.textAlignment = .center
             lb_subtitle.numberOfLines = 0
             lb_subtitle.text = singledict["subtitle"]
             lb_subtitle.font = UIFont (name: "Poppins-Light", size: 15)
            View_subView.addSubview(lb_subtitle)
           
            
          
         
        }
                
       
      
        //ScPageControl.frame = CGRect(x: 0, y: screenHeight-150, width: screenWidth, height: 50)
        ScPageControl.scp_style = .SCJAMoveCircle
        ScPageControl.set_view(Int(sc_main.contentSize.width/screenWidth), current: 0, current_color: #colorLiteral(red: 0.9519053102, green: 0.6995219588, blue: 0.007269139402, alpha: 1), disable_color: .gray)
        
     
    }
    func calculatePercentage(max_width:Double,percentageVal:Double)->Double{
       let val = max_width * percentageVal
       return val / 100.0
   }
    //MARK: ## ScrollView Delegate ##
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //Added as required
        ScPageControl.scroll_did(scrollView)
       
    }
    
    // ## Moment in rotate Device ##
    // Only when not use AutoLayout
    @objc func deviceDidRotate() {
        
        if UIDevice.current.orientation == previousDeviceOrientation { return }
        previousDeviceOrientation = UIDevice.current.orientation
        
        screenWidth = UIScreen.main.bounds.size.width
        screenHeight = UIScreen.main.bounds.size.height
        
        var f_x: CGFloat = 0.0
        for subview in sc_main.subviews {
            if subview.isKind(of: UILabel.classForCoder()) {
                subview.frame = CGRect(x: f_x, y: 100, width: screenWidth, height: 100)
                f_x += screenWidth
            }
        }
        let f_page = sc_main.contentOffset.x / sc_main.frame.size.width
        
        sc_main.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        sc_main.contentSize = CGSize(width: screenWidth*2.0, height: screenHeight)
        
        ScPageControl.frame = CGRect(x: 0, y: screenHeight-50, width: screenWidth, height: 50)
        ScPageControl.set_rotateDevice()
        
       
          
        sc_main.scrollRectToVisible(CGRect(x: sc_main.frame.size.width*f_page, y: 0, width:  sc_main.frame.size.width, height:  sc_main.frame.size.height), animated: true)
    }
  
}

/*enum Pages: CaseIterable {
   
    case pageOne
    case pageTwo
    case pageThree
    //case pageFour
    var name: String {
        switch self {
        case .pageOne:
            return "One"
        case .pageTwo:
            return "Two"
        case .pageThree:
            return "Three"
        //case .pageFour:
         //   return "Four"
        }
    }
    
    var imgname: String {
        switch self {
        case .pageOne:
            return "onboard3"
        case .pageTwo:
            return "onboard2"
        case .pageThree:
            return "onboard1"
        //case .pageFour:
         //   return "Four"
        }
    }
    var index: Int {
        switch self {
        case .pageOne:
            return 0
        case .pageTwo:
            return 1
        case .pageThree:
            return 2
       // case .pageFour:
         //   return 3
        }
    }
}

class OnbordingViewController: UIViewController {
    
    @IBOutlet weak var btn_next: UIButton!
    
     
    @IBOutlet weak var ScPageControl: SCPageControlView!
    
    @IBOutlet weak var circularProgress: CircularProgressView!
    private var pageController: UIPageViewController?
    private var pages: [Pages] = Pages.allCases
    private var currentIndex: Int = 0

  
    
    
    var progress : Double = 0.25
    var screenWidth : CGFloat = UIScreen.main.bounds.size.width
    var screenHeight : CGFloat = UIScreen.main.bounds.size.height
  
    @IBOutlet weak var btn_Skip: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.view.backgroundColor = .white
        
        self.setupPageController()
        ScPageControl.frame = CGRect(x: 0, y: screenHeight-150, width: screenWidth, height: 30)
        ScPageControl.scp_style = .SCNormal
        ScPageControl.set_view(3, current: 0, current_color: .green, disable_color: .gray)
       // perform(#selector(startUpload), with: nil, afterDelay: 0.10)
       // circularProgress.addSubview(btn_next)
    }
    @objc func startUpload() {
//        circularProgress.labelSize = 30
//        circularProgress.safePercent = 100
//        circularProgress.setProgress(to: progress, withAnimation: true)
//        circularProgress.delegate = self
        //let timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateProgress), userInfo: nil, repeats: true)
        //timer.fire()
       // RunLoop.main.add(timer, forMode: .common)
    }

    @IBAction func btn_inc(_ sender: Any) {
//        progress = progress + 0.25
//        circularProgress.setProgress(to: progress, withAnimation: true)
//
    }
    @IBAction func btn_Skip(_ sender: Any) {
//        progress = progress + 0.25
//        circularProgress.setProgress(to: progress, withAnimation: true)
 
        if let myDelegate = (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate){
            
         
            UserDefaults.standard.set(Mishi_methods.senderId, forKey: "user_Details")
            myDelegate.manageLoginSession()
            
        }
    }
    
    private func setupPageController() {
        
        self.pageController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        self.pageController?.dataSource = self
        self.pageController?.delegate = self
        
        self.pageController?.view.backgroundColor = .clear
        self.pageController?.view.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: self.view.frame.height-200)
        self.addChild(self.pageController!)
        self.view.addSubview(self.pageController!.view)
        
        let initialVC = PageVC(with: pages[0])
        
        self.pageController?.setViewControllers([initialVC], direction: .forward, animated: true, completion: nil)
        
        self.pageController?.didMove(toParent: self)
        view.bringSubviewToFront(btn_Skip)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       
    }
}

extension OnbordingViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    private func pageViewController(pageViewController: UIPageViewController,
           didFinishAnimating finished: Bool,
           previousViewControllers: [UIViewController],
           transitionCompleted completed: Bool) {
        
 print("After commmmmmm")
       }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let currentVC = viewController as? PageVC else {
            return nil
        }
        
        var index = currentVC.page.index
        
        if index == 0 {
            return nil
        }
        
        index -= 1
        
        //print("pagenum-dec:",index)

        
        let vc: PageVC = PageVC(with: pages[index])
        vc.delegate = self
       // progress = progress - 0.25
       // circularProgress.setProgress(to: progress, withAnimation: true)
      
        return vc
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let currentVC = viewController as? PageVC else {
            return nil
        }
        
        var index = currentVC.page.index
        
        if index >= self.pages.count - 1 {
            return nil
        }
        
        index += 1
         
       //print("pagenum-inc:",index)

        let vc: PageVC = PageVC(with: pages[index])
        vc.delegate = self
       // progress = progress + 0.25
        //circularProgress.setProgress(to: progress, withAnimation: true)
      
        return vc
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return self.pages.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        //print("current",self.currentIndex)
        return self.currentIndex
    }
}
extension OnbordingViewController: pageDelegate {
    func pageswipe_method(_ ChatVideo: PageVC, Singleitem: Pages) {
        print(Singleitem.name)
        
        print(Singleitem.index)
        //ScPageControl.scroll_did(Singleitem.index)
        /*if Singleitem.name == "One"{
            progress = 0.25
        }
        else if Singleitem.name  == "Two" {
            progress = 0.50
        }
        else if Singleitem.name  == "Three" {
            progress = 0.75
        }
        else if Singleitem.name  == "Four" {
            progress = 1
        }
       
         circularProgress.setProgress(to: progress, withAnimation: true)*/
       
    }
    
}
extension OnbordingViewController: CircleviewDelegate {
    func btnNext_method(_ ChatVideo: CircularProgressView)
   
    {
        print("pressed")
        //let initialVC = PageVC(with: pages[self.currentIndex+1])
      
        //let pageController = self.pageController as! PageViewController
        self.pageController?.goToNextPage()
        //pageController!.setViewControllers([pageController.orderedViewControllers[1]], direction: .forward, animated: true, completion: nil)

        //self.pageController.
    }
    
}
extension UIPageViewController {

    func goToNextPage() {
       guard let currentViewController = self.viewControllers?.first else { return }
       guard let nextViewController = dataSource?.pageViewController( self, viewControllerAfter: currentViewController ) else { return }
   
       setViewControllers([nextViewController], direction: .forward, animated: true, completion: nil)
    }

    func goToPreviousPage() {
       guard let currentViewController = self.viewControllers?.first else { return }
       guard let previousViewController = dataSource?.pageViewController( self, viewControllerBefore: currentViewController ) else { return }
       setViewControllers([previousViewController], direction: .reverse, animated: true, completion: nil)
    }

}*/
/*class OnbordingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}*/
