//
//  Login.swift
//  MvvmDemoApp
//
//  Created by Codecat15 on 3/14/2020.
//  Copyright © 2020 Codecat15. All rights reserved.
//

import Foundation

struct LoginResponse_total  {
    let  message:String?
    let  data:LoginResponse_json?
   
      
}
struct LoginResponse_json  {

    let token: String
    let userId: String
    let username: String
    let firstname: String
    let lastname: String
    let is_staff: Bool
    let organization: String
    let org_id: String
      
}







struct LoginError : Decodable{
    let  message:String
}
struct LoginResponse : Decodable {

    let token: String
    let userId: String
    let username: String
    let firstname: String
    let lastname: String
    let is_staff: Bool
    let organization: String
    let org_id: String
      
}

struct LoginResponseData : Decodable
{
    let token: String
    let userId: String
    let username: String
    let firstname: String
    let lastname: String
    let is_staff: Bool
    let organization: String
    let org_id: String
      
    /*enum CodingKeys: String, CodingKey {
        case token
        case userId
        case username
        case firstname
        case lastname
        case is_staff
        case organization
        case org_id
    }*/
}
