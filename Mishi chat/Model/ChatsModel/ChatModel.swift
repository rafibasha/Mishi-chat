//
//  ChatModel.swift
//  Mishi chat
//
//  Created by Rafi Basha on 10/08/21.
//

import Foundation

struct ChatResponse  {
    let  id:String?
    let  data:[Chatsingleitem]?
   
      
}
struct Chatsingleitem  {

    let author: String
    let message: String
    let channel:String
    let status:String
    let sub_date:String
}
