//
//  BotsViewModel.swift
//  Mishi chat
//
//  Created by Rafi Basha on 10/08/21.
//

import Foundation
protocol BotsViewModelDelegate {
    func didReceiveTicketsResponse(botsResponse: [BotsResponse]?)
    func didReceiveTicketsFailed(botsResponse: String?)
}

struct BotsViewModel
{
    var delegate : BotsViewModelDelegate?
    
    func getbots(botRequest: String)
    {
        
        let botUrl = URL(string: botRequest)!
        let httpUtility = HttpUtility()
        
        
        
        httpUtility.getBotsData(requestUrl: botUrl){
            loginApiResponse in
            
            print("loginApiResponse",loginApiResponse)
            DispatchQueue.main.async {
                self.delegate?.didReceiveTicketsResponse(botsResponse: loginApiResponse)
            }
        }

        
    }
    
    
}

