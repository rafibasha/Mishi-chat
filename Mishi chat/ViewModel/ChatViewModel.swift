//
//  ChatViewModel.swift
//  Mishi chat
//
//  Created by Rafi Basha on 10/08/21.
//

import Foundation
protocol ChatViewModelDelegate {
    func didReceiveChatsResponse(Chatdata: [ChatResponse]?)
    func didReceiveChatsFailed(Chatdata: String?)
}

struct ChatViewModel
{
    var delegate : ChatViewModelDelegate?
    
    func getChats(botRequest: String)
    {
        
        let botUrl = URL(string: botRequest)!
        let httpUtility = HttpUtility()
        
        
        
        httpUtility.getChatsData(requestUrl: botUrl){
            loginApiResponse in
            
           
            DispatchQueue.main.async {
                self.delegate?.didReceiveChatsResponse(Chatdata: loginApiResponse)
            }
        }

        
    }
    
    
}
