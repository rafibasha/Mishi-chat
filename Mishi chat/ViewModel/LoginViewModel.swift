//
//  LoginViewModel.swift
//  MvvmDemoApp
//
//  Created by Codecat15 on 3/14/2020.
//  Copyright © 2020 Codecat15. All rights reserved.
//

import Foundation

protocol LoginViewModelDelegate {
    func didReceiveLoginResponse(loginResponse: LoginResponse_json?)
    func didReceiveLoginFailed(loginResponse: String?)
}

struct LoginViewModel
{
    var delegate : LoginViewModelDelegate?

    func loginUser(loginRequest: LoginRequest)
    {
        let validationResult = LoginValidation().Validate(loginRequest: loginRequest)

        if(validationResult.success)
        {
            //use loginResource to call login API
            let loginResource = LoginResource()
            loginResource.loginUser(loginRequest: loginRequest) { (loginApiResponse) in

              
                if loginApiResponse.message == "Success"{
                    
                    guard let response = loginApiResponse.data else {
                        return
                    }
                   
                    //return the response we get from loginResource
                    DispatchQueue.main.async {
                        self.delegate?.didReceiveLoginResponse(loginResponse: response)
                    }
                }
                else{
                    guard let response = loginApiResponse.message else {
                        return
                    }
                  
                    DispatchQueue.main.async {
                        self.delegate?.didReceiveLoginFailed(loginResponse: response)
                    }
                }
 
            }
        }
        else{
            self.delegate?.didReceiveLoginFailed(loginResponse: nil)
        }
       
    }
}
