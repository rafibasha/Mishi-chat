//
//  Common.swift
//  MvvmDemoApp
//
//  Created by CodeCat15 on 3/14/20.
//  Copyright © 2020 Codecat15. All rights reserved.
//

import Foundation

struct Constants {
    static let ErrorAlertTitle = "Error"
    static let OkAlertTitle = "Ok"
    static let CancelAlertTitle = "Cancel"
}

struct ApiEndpoints
{
    static let login = "https://commerce.mishi.ai/chatcentre/login"
    static let bots = "https://commerce.mishi.ai/chatcentre/organization/"
    static let botsChats = "https://commerce.mishi.ai/chatcentre/conversation/"
}
